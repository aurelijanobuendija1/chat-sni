package api

import (
	"chat-server/models"
	"chat-server/util"
	"encoding/json"
	"net/http"
)

type VerifyRequest struct {
	Code      string `json:"code" validate:"required" schema:"code"`
	TempToken string `json:"temp_token" validate:"required" schema:"temp_token"`
}

type VerifyResponse struct {
	User models.User `json:"user"`
}

func Verify(w http.ResponseWriter, r *http.Request) {
	var body VerifyRequest

	err := util.ParseFormFromRequest(r, &body)
	if err != nil {
		response := ErrorResponse{
			Error:       "Bad login",
			Description: err.Error(),
		}
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(response)
		return
	}

	if util.CheckForXSS(body.Code) || util.CheckForSqlInjection(body.Code) {
		response := RegisterResponse{
			Status: "Bad request",
			Error:  "Malicious attempt",
		}

		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(response)
		return
	}

	var tempToken models.TempToken
	if err := models.DB.Preload("User").Where("id = ? AND PIN = ?", body.TempToken, body.Code).First(&tempToken).Error; err != nil {
		response := ErrorResponse{
			Error:       "Invalid request",
			Description: err.Error(),
		}

		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(response)
		return
	}

	user := tempToken.User

	var session models.Session
	if err := models.DB.Where("user_id = ?", user.ID).First(&session).Error; err != nil {
		newSession := models.Session{
			UserID: user.ID,
		}

		if err := models.DB.Create(&newSession).Error; err != nil {
			response := ErrorResponse{
				Error:       "Cannot create session",
				Description: "",
			}

			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(response)
			return
		}

		session = newSession
	}

	http.SetCookie(w, &http.Cookie{
		Name:   "session_id",
		Value:  session.ID,
		Domain: "chat.example.com",
		Path:   "/",
		Secure: true,
		//HttpOnly: true,
		SameSite: http.SameSiteNoneMode,
	})
	http.SetCookie(w, &http.Cookie{
		Name:   "temp_token",
		Value:  "",
		Domain: "chat.example.com",
		Path:   "/",
		Secure: true,
		//HttpOnly: true,
		SameSite: http.SameSiteNoneMode,
	})

	models.DB.Where("id = ? AND PIN = ?", body.TempToken, body.Code).Delete(&models.TempToken{})

	response := LoginResponse{
		User:      user,
		SessionID: session.ID,
	}

	json.NewEncoder(w).Encode(response)
	return
}
