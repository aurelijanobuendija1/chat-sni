package api

import (
	"chat-server/models"
	"chat-server/util"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"net/http"
)

type LoginRequest struct {
	Email         string `json:"email" validate:"required" schema:"email"`
	Password      string `json:"password" validate:"required" schema:"password"`
	RecaptchaData string `json:"recaptcha_data" validate:"required" schema:"recaptcha_data"`
}

func (r *LoginRequest) CheckMalicious() bool {
	return util.TestMaliciousArray([]string{r.Email, r.Password})
}

type LoginResponse struct {
	User      models.User `json:"user"`
	SessionID string      `json:"session_id"`
	TempToken string      `json:"temp_token"`
}

func Login(w http.ResponseWriter, r *http.Request) {
	cn := r.Header.Get("X-Cn")

	var body LoginRequest

	err := util.ParseFormFromRequest(r, &body)
	if err != nil {
		response := ErrorResponse{
			Error:       "Bad login",
			Description: err.Error(),
		}

		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(response)
		return
	}

	if err := util.CheckRecaptcha(body.RecaptchaData); err != nil {
		response := ErrorResponse{
			Error:       "Bad login",
			Description: err.Error(),
		}

		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(response)
		return
	}

	if body.CheckMalicious() {
		response := RegisterResponse{
			Status: "Bad request",
			Error:  "Malicious attempt",
		}

		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(response)
		return
	}

	var user models.User
	if err := models.DB.Where("email = ?", body.Email).First(&user).Error; err != nil {
		response := ErrorResponse{
			Error:       "User not found",
			Description: err.Error(),
		}

		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(response)
		return
	}

	if cn != user.Email {
		response := ErrorResponse{
			Error:       "Bad certificate",
			Description: "Bad certificate",
		}

		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(response)
		return
	}

	if user.Blocked {
		response := ErrorResponse{
			Error:       "Blocked",
			Description: "User has been blocked due malicious activity",
		}

		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(response)
		return
	}

	passByte := []byte(body.Password)
	hash := sha256.Sum256(passByte)
	if user.Password != hex.EncodeToString(hash[:]) {
		response := ErrorResponse{
			Error:       "User not found",
			Description: "",
		}
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(response)
		return
	}

	tempToken, err := getTempToken(user)
	if err != nil {
		response := ErrorResponse{
			Error:       "Cannot create token",
			Description: "",
		}

		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(response)
		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:  "temp_token",
		Value: tempToken.ID,
	})

	response := LoginResponse{
		User:      user,
		TempToken: tempToken.ID,
	}

	util.SendMail(user.Email, tempToken.PIN)

	json.NewEncoder(w).Encode(response)
	return
}

func getTempToken(user models.User) (*models.TempToken, error) {
	tempToken := models.TempToken{UserID: user.ID}
	err := models.DB.Create(&tempToken).Error
	if err != nil {
		return nil, err
	}

	return &tempToken, err
}
