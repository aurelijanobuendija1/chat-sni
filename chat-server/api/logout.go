package api

import (
	"chat-server/models"
	"net/http"
)

func Logout(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie("session_id")
	if err != nil {

	} else {
		var session models.Session
		models.DB.Where("id = ?", cookie.Value).Delete(&session)
	}
	http.SetCookie(w, &http.Cookie{
		Name:   "session_id",
		Value:  "",
		Domain: "chat.example.com",
		Path:   "/",
		Secure: true,
		MaxAge: -1,
		//HttpOnly: true,
		SameSite: http.SameSiteNoneMode,
	})
	http.SetCookie(w, &http.Cookie{
		Name:   "temp_token",
		Value:  "",
		Domain: "chat.example.com",
		Path:   "/",
		Secure: true,
		MaxAge: -1,
		//HttpOnly: true,
		SameSite: http.SameSiteNoneMode,
	})
	w.WriteHeader(http.StatusOK)
	return
}