package api

import (
	"chat-server/models"
	"encoding/json"
	"net/http"
)

func GetProfile(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie("session_id")
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode("Go to login")
		return
	}
	var session models.Session
	if err := models.DB.Preload("User").Where("id = ?", cookie.Value).First(&session).Error; err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode("Go to login")
		return
	}

	response := LoginResponse{
		User: session.User,
		SessionID: session.ID,
	}

	json.NewEncoder(w).Encode(response)
	return
}
