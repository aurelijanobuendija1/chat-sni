package api

import (
	"chat-server/criptoutil"
	"chat-server/models"
	"chat-server/util"
	"encoding/json"
	"fmt"
	"net/http"
)

type RegisterRequest struct {
	models.User
	RecaptchaData string `json:"recaptcha_data" validate:"required" schema:"recaptcha_data"`
}

type RegisterResponse struct {
	Certificate string `json:"certificate"`
	Status      string `json:"status"`
	Error       string `json:"error"`
}

func Register(w http.ResponseWriter, r *http.Request) {
	var body RegisterRequest

	err := util.ParseFormFromRequest(r, &body)
	if err != nil {
		response := RegisterResponse{
			Status: "Bad request",
			Error:  err.Error(),
		}

		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(response)
		return
	}

	if err := util.CheckRecaptcha(body.RecaptchaData); err != nil {
		response := ErrorResponse{
			Error:       "Bad login",
			Description: err.Error(),
		}

		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(response)
		return
	}

	user := body.User

	if user.CheckMalicious() {
		response := RegisterResponse{
			Status: "Bad request",
			Error:  "Malicious attempt",
		}

		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(response)
		return
	}

	pass := user.Password
	user.HashPassword()

	if err := models.DB.Create(&user).Error; err != nil {
		response := RegisterResponse{
			Status: "409",
			Error:  "Email is already in use",
		}
		w.WriteHeader(http.StatusConflict)
		json.NewEncoder(w).Encode(response)
		return
	}

	filename := criptoutil.CreateUserCertificateEasy(user, pass)

	w.Header().Set("Content-Disposition", "attachment; filename="+fmt.Sprintf("%s-certificate.p12", user.Email))
	w.Header().Set("Content-Type", "application/octet-stream")
	http.ServeFile(w, r, filename)

	return
}
