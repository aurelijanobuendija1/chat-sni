module chat-server

go 1.15

require (
	github.com/go-playground/validator/v10 v10.4.1
	github.com/googollee/go-socket.io v1.4.4
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/schema v1.2.0
	github.com/gorilla/websocket v1.4.2
	github.com/joho/godotenv v1.3.0
	github.com/satori/go.uuid v1.2.0
	gorm.io/driver/mysql v1.0.4
	gorm.io/gorm v1.20.12
	software.sslmate.com/src/go-pkcs12 v0.0.0-20201103104416-57fc603b7f52
)
