package models

import (
	"chat-server/util"
	"crypto/sha256"
	"encoding/hex"
	"time"

	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

type User struct {
	ID        string    `gorm:"primaryKey;type:string" json:"id"`
	FirstName string    `json:"first_name" validate:"required,max=20" schema:"first_name,required" `
	LastName  string    `json:"last_name" validate:"required,max=20" schema:"last_name,required"`
	Email     string    `gorm:"unique" json:"email" validate:"required,email" schema:"email,required"`
	Password  string    `json:"-" validate:"required,min=8,max=20" schema:"password,required"`
	Blocked   bool      `gorm:"nullable" json:"blocked"`
	CreatedAt time.Time `json:"created_at"`
}

func (u *User) BeforeCreate(tx *gorm.DB) (err error) {
	u.ID = uuid.NewV4().String()

	return
}

//HashPassword - takes Password value and sets it to its hash value
func (u *User) HashPassword() {
	passByte := []byte(u.Password)
	hash := sha256.Sum256(passByte)
	u.Password = hex.EncodeToString(hash[:])
}

func (u *User) BlockAndTerminate() {
	u.Blocked = true
	DB.Save(&u)
	DB.Where("user_id = ?", u.ID).Delete(&Session{})
	DB.Where("user_id = ?", u.ID).Delete(&TempToken{})
	return
}

func (u *User) CheckMalicious() bool {
	return util.TestMaliciousArray([]string{u.FirstName, u.LastName, u.Email, u.Password})
}
