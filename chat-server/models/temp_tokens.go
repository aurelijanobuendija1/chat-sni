package models

import (
	"chat-server/util"
	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
	"os"
	"strconv"
	"time"
)

//TempToken - temp_token model
type TempToken struct {
	ID           string `gorm:"primaryKey;type:string"`
	PIN          string
	UserID       string `gorm:"unique"`
	User         User   /*`gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`*/
	CreatedAt    time.Time
}

//BeforeCreate - uuid hook
func (t *TempToken) BeforeCreate(tx *gorm.DB) (err error) {
	t.ID = uuid.NewV4().String()
	t.PIN = util.GeneratePin()

	return
}

func (t *TempToken) isExpired() bool {
	allowed, err := strconv.ParseFloat(os.Getenv("TEMP_TOKEN_DURATION_IN_MINUTES"), 64)
	if err != nil {
		allowed = 5
	}
	return time.Now().Sub(t.CreatedAt).Minutes() > allowed
}
