package models

import "chat-server/util"

type Message struct {
	Content   string `json:"content" validate:"required"`
	From      string `json:"from" validate:"required"`
	To        string `json:"to" validate:"required"`
	Timestamp string `json:"timestamp" validate:"required"`
}

func (m *Message) CheckForXSS() bool {
	return util.CheckForXSS(m.Content) || util.CheckForXSS(m.From) || util.CheckForXSS(m.To) || util.CheckForXSS(m.Timestamp)
}

func (m *Message) CheckForSQLInjection() bool {
	return util.CheckForSqlInjection(m.Content) || util.CheckForSqlInjection(m.From) || util.CheckForSqlInjection(m.To) || util.CheckForSqlInjection(m.Timestamp)
}
