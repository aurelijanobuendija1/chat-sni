package models

import (
	"time"

	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

//Session - session model
type Session struct {
	ID           string `gorm:"primaryKey;type:string"`
	ConnectionID string `gorm:"unique,nullable;type:string"`
	UserID       string `gorm:"unique"`
	User         User
	CreatedAt    time.Time
}

//BeforeCreate - uuid hook
func (s *Session) BeforeCreate(tx *gorm.DB) (err error) {
	s.ID = uuid.NewV4().String()

	return
}
