package util

import (
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/gorilla/schema"
	"net/http"
)

var decoder = schema.NewDecoder()
var Validate = validator.New()

// ParseFormFromRequest parses form body into desirable struct
func ParseFormFromRequest(r *http.Request, body interface{}) error {
	err := r.ParseForm()
	if err != nil {
		return err
	}

	err = decoder.Decode(body, r.PostForm)
	if err != nil {
		fmt.Println(err.Error())
		return err
	}

	err = Validate.Struct(body)
	if err != nil {
		return err
	}

	return nil
}
