package util

import (
	"crypto/rand"
	"io"
)

const CodeLength = 6
func getAlphabet() []byte {
	return []byte{'0','1','2', '3','4','5','6','7','8', '9'}
}
func GeneratePin() string {
	b := make([]byte, CodeLength)
	n, err := io.ReadAtLeast(rand.Reader, b, CodeLength)
	if n != CodeLength {
		panic(err)
	}
	for i := 0; i < len(b); i++ {
		b[i] = getAlphabet()[int(b[i])%len(getAlphabet())]
	}
	return string(b)
}
