package util

import (
	"fmt"
	"log"
	"net/smtp"
	"os"
)

func SendMail(userMail string, code string) {
	auth := smtp.PlainAuth("", os.Getenv("MAIL_USERNAME"), os.Getenv("MAIL_PASSWORD"), os.Getenv("MAIL_HOST"))

	to := []string{userMail}
	msg := []byte(fmt.Sprintf("From: chat@chat.com\r\n"+
		"To: %s\r\n"+
		"Subject: Verification code\r\n"+
		"\r\n"+
		"Your verification code is: %s \r\n", userMail, code))
	err := smtp.SendMail(fmt.Sprintf("%s:%s", os.Getenv("MAIL_HOST"), os.Getenv("MAIL_PORT")), auth, "chat@chat.com", to, msg)
	if err != nil {
		log.Fatal(err)
	}
}
