package main

import (
	"chat-server/api"
	"fmt"
	"github.com/gorilla/mux"
	_ "github.com/joho/godotenv/autoload"
	"log"
	"net/http"
	"os"
)

func serveAPI() {
	if err := http.ListenAndServe(fmt.Sprintf(":%s", os.Getenv("SERVER_PORT")), corsMiddleware(getRouter())); err != nil {
		fmt.Println(err.Error())
	}
}

func getRouter() *mux.Router {
	router := mux.NewRouter()

	router.HandleFunc("/api/login", api.Login).Methods(http.MethodPost)
	router.HandleFunc("/api/verify", tempTokenMiddleware(api.Verify)).Methods(http.MethodPost)
	router.HandleFunc("/api/register", api.Register).Methods(http.MethodPost)
	router.HandleFunc("/api/profile", api.GetProfile).Methods(http.MethodGet)
	router.HandleFunc("/api/logout", api.Logout).Methods(http.MethodPost)

	return router
}

func main() {
	server := getSocketServer()

	go server.Serve()
	defer server.Close()

	setSql()
	go func() {
		serveAPI()
	}()

	http.Handle("/socket.io/", corsMiddleware(sessionIDMiddleware(server)))
	log.Println("Serving at localhost:8000...")
	log.Fatal(http.ListenAndServe(":8000", nil))
}
