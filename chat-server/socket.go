package main

import (
	"chat-server/models"
	"encoding/json"
	"errors"
	"fmt"
	socketio "github.com/googollee/go-socket.io"
	"github.com/googollee/go-socket.io/engineio"
	"github.com/googollee/go-socket.io/engineio/transport"
	"github.com/googollee/go-socket.io/engineio/transport/websocket"
	"net/http"
)

func getSocketServer() *socketio.Server {
	onlineUsers := map[string]models.User{}
	connectionToUser := map[string]models.User{}
	userToConnection := map[string]socketio.Conn{}
	userToConnections := map[string][]socketio.Conn{}
	userToLastMessageTimestamp := map[string]string{}
	userToChillOutCount := map[string]int{}


	server, err := socketio.NewServer(&engineio.Options{
		Transports: []transport.Transport{
			websocket.Default,
			&websocket.Transport{
				CheckOrigin: func(r *http.Request) bool {
					return true
				},
			},
		},
		RequestChecker: func(request *http.Request) (http.Header, error) {
			sessionID := request.URL.Query().Get("session_id")
			var session models.Session
			if err := models.DB.Where("id = ?", sessionID).First(&session).Error; err != nil {
				return nil, errors.New("not authorized")
			}
			return nil, nil
		},
	})
	if err != nil {
		fmt.Println(err)
	}

	server.OnConnect("/", func(s socketio.Conn) error {
		fmt.Println("connected")
		url := s.URL()
		sessionID := url.Query().Get("session_id")

		var session models.Session
		if err := models.DB.Where("id = ?", sessionID).First(&session).Error; err != nil {
			return err
		} else {
			s.Join("onlineUsers")
			fmt.Println("connected:", s.ID())
			session.ConnectionID = s.ID()
			models.DB.Save(&session)

			var user models.User
			models.DB.Where("id = ?", session.UserID).First(&user)

			onlineUsers[user.ID] = user
			connectionToUser[s.ID()] = user
			userToConnection[user.ID] = s
			userToConnections[user.ID] = append(userToConnections[user.ID], s)
			userToLastMessageTimestamp[user.ID] = ""

			jsonUsers, _ := json.Marshal(onlineUsers)
			s.Emit("onlineUsers", string(jsonUsers))

			jsonNewUser, _ := json.Marshal(user)
			server.BroadcastToRoom("/", "onlineUsers", "addOnlineUser", string(jsonNewUser))
			return nil
		}

		return errors.New("not authorized")

	})

	server.OnEvent("/", "msg", func(s socketio.Conn, msg string) string {
		s.SetContext(msg)
		var message models.Message
		err := json.Unmarshal([]byte(msg), &message)
		if err != nil {
			fmt.Println(err.Error())
		}
		user := connectionToUser[s.ID()]

		if message.Timestamp == userToLastMessageTimestamp[message.From] {
			//TODO: remove code duplication
			userToChillOutCount[message.From]++
			if userToChillOutCount[message.From] > 3 {
				user.BlockAndTerminate()

				s.Emit("malicious", "Your account has been blocked due to an DoS attack attempt.")

				server.BroadcastToRoom("/", "onlineUsers", "removeOnlineUser", connectionToUser[s.ID()].ID)
				models.DB.Where("user_id = ?", user.ID).Delete(models.Session{})
				delete(connectionToUser, s.ID())

				userToConnections[user.ID] = removeFromArray(userToConnections[user.ID], s.ID())
				if len(userToConnections[user.ID]) == 0 {
					delete(userToConnections,user.ID)
					delete(onlineUsers, user.ID)
				}
				s.Leave("onlineUser")
				_ = s.Close()
				server.BroadcastToRoom("/", "onlineUsers", "removeOnlineUser", connectionToUser[s.ID()].ID)
				return "malicious"
			}
			return fmt.Sprintf("Chill out (Warning no: %d)", userToChillOutCount[message.From])
		}

		if message.CheckForXSS() {
			user.BlockAndTerminate()

			s.Emit("malicious", "Your account has been blocked due to an XSS attack attempt.")

			server.BroadcastToRoom("/", "onlineUsers", "removeOnlineUser", connectionToUser[s.ID()].ID)
			models.DB.Where("user_id = ?", user.ID).Delete(models.Session{})
			delete(connectionToUser, s.ID())

			userToConnections[user.ID] = removeFromArray(userToConnections[user.ID], s.ID())
			if len(userToConnections[user.ID]) == 0 {
				delete(userToConnections,user.ID)
				delete(onlineUsers, user.ID)
			}
			s.Leave("onlineUser")
			_ = s.Close()
			server.BroadcastToRoom("/", "onlineUsers", "removeOnlineUser", connectionToUser[s.ID()].ID)
			return "malicious"
		}

		if message.CheckForSQLInjection() {
			user.BlockAndTerminate()
			s.Emit("malicious", "Your account has been blocked due to an SQL injection attack attempt.")
			server.BroadcastToRoom("/", "onlineUsers", "removeOnlineUser", connectionToUser[s.ID()].ID)
			models.DB.Where("user_id = ?", user.ID).Delete(models.Session{})
			delete(connectionToUser, s.ID())

			userToConnections[user.ID] = removeFromArray(userToConnections[user.ID], s.ID())
			if len(userToConnections[user.ID]) == 0 {
				delete(userToConnections,user.ID)
				delete(onlineUsers, user.ID)
			}
			s.Leave("onlineUser")
			_ = s.Close()
			server.BroadcastToRoom("/", "onlineUsers", "removeOnlineUser", connectionToUser[s.ID()].ID)

			return "malicious"
		}

		userToLastMessageTimestamp[message.From] = message.Timestamp
		//userToConnection[message.To].Emit("new_message", msg )
		for _, conn := range userToConnections[message.To] {
			conn.Emit("new_message", msg)
		}

		return "ack"
	})

	server.OnEvent("/", "bye", func(s socketio.Conn, msg string) string {
		s.SetContext(msg)
		user := connectionToUser[s.ID()]
		server.BroadcastToRoom("/", "onlineUsers", "removeOnlineUser", connectionToUser[s.ID()].ID)
		models.DB.Where("user_id = ?", user.ID).Delete(models.Session{})
		delete(connectionToUser, s.ID())

		userToConnections[user.ID] = removeFromArray(userToConnections[user.ID], s.ID())
		if len(userToConnections[user.ID]) == 0 {
			delete(userToConnections,user.ID)
			delete(onlineUsers, user.ID)
		}
		s.Leave("onlineUser")
		_ = s.Close()
		server.BroadcastToRoom("/", "onlineUsers", "removeOnlineUser", connectionToUser[s.ID()].ID)
		return "ack"
	})

	server.OnError("/", func(s socketio.Conn, e error) {
		fmt.Println("meet error:", e)
	})

	server.OnDisconnect("/", func(s socketio.Conn, reason string) {
		s.Leave("onlineUsers")
		user := connectionToUser[s.ID()]
		delete(connectionToUser, s.ID())

		userToConnections[user.ID] = removeFromArray(userToConnections[user.ID], s.ID())
		if len(userToConnections[user.ID]) == 0 {
			delete(userToConnections,user.ID)
			delete(onlineUsers, user.ID)
			server.BroadcastToRoom("/", "onlineUsers", "removeOnlineUser", user.ID)
		}

		fmt.Println("closed", reason)
	})

	return server
}

func removeFromArray(arr []socketio.Conn, value string) (ret []socketio.Conn) {
	for _, el := range arr {
		if el.ID() != value{
			ret = append(ret, el)
		}
	}
	return
}
