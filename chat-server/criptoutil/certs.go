package criptoutil

import (
	"bytes"
	"chat-server/models"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"math/big"
	"net"
	"os"
	"os/exec"
	"software.sslmate.com/src/go-pkcs12"
	"time"
)

func CreateCA() {
	ca := &x509.Certificate{
		SerialNumber: big.NewInt(2019),
		Subject: pkix.Name{
			Organization:  []string{"Limes Chat"},
			Country:       []string{"BA"},
			Province:      []string{"RS"},
			Locality:      []string{"Banja Luka"},
			StreetAddress: []string{"Sinana Sakica 666"},
			PostalCode:    []string{"78000"},
		},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().AddDate(10, 0, 0),
		IsCA:                  true,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth},
		KeyUsage:              x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign | x509.KeyUsageCRLSign | x509.KeyUsageDataEncipherment,
		BasicConstraintsValid: true,
	}

	caPrivKey, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		fmt.Printf("Error: private key: %s\n", err.Error())
	}

	caBytes, err := x509.CreateCertificate(rand.Reader, ca, ca, &caPrivKey.PublicKey, caPrivKey)
	if err != nil {
		fmt.Printf("Error: ca cert create: %s\n", err.Error())
	}

	caPEM := new(bytes.Buffer)
	pem.Encode(caPEM, &pem.Block{
		Type:  "CERTIFICATE",
		Bytes: caBytes,
	})

	caPrivKeyPEM := new(bytes.Buffer)
	pem.Encode(caPrivKeyPEM, &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(caPrivKey),
	})

	err = ioutil.WriteFile("cacert.pem", caPEM.Bytes(), os.ModePerm)
	if err != nil {
		fmt.Println("Cannot write cert to file")
	}

	err = ioutil.WriteFile("private.pem", caPrivKeyPEM.Bytes(), os.ModePerm)
	if err != nil {
		fmt.Println("Cannot write private key to file")
	}
}

func CreateUserCertificate(user models.User) []byte {
	cert := &x509.Certificate{
		SerialNumber: big.NewInt(1658),
		Subject: pkix.Name{
			//Organization:  []string{"Limes Chat"},
			//Country:       []string{"BA"},
			//Province:      []string{"RS"},
			//Locality:      []string{"Banja Luka"},
			//StreetAddress: []string{"Sinana Sakica 666"},
			//PostalCode:    []string{"78000"},
			CommonName: fmt.Sprintf("%s.client.chat.example.com", user.ID),
		},
		IPAddresses:  []net.IP{net.IPv4(127, 0, 0, 1), net.IPv6loopback},
		NotBefore:    time.Now(),
		NotAfter:     time.Now().AddDate(10, 0, 0),
		SubjectKeyId: []byte (user.ID),
		ExtKeyUsage:  []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth},
		KeyUsage:     x509.KeyUsageDigitalSignature | x509.KeyUsageDataEncipherment,
	}

	certPrivKey, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		panic(fmt.Sprintf("Error: user private key: %s\n", err.Error()))
	}

	ca, err := ioutil.ReadFile("certs/ca.pem")
	if err != nil {
		panic(fmt.Sprintf("Error: cannot read ca cert: %s\n", err.Error()))
	}

	der, _ := pem.Decode(ca)

	CACert, err := x509.ParseCertificate(der.Bytes)
	if err != nil {
		panic(fmt.Sprintf("Error: cannot parse ca cert: %s\n", err.Error()))
	}

	CAPrivateKey, err := ioutil.ReadFile("certs/ca.key")
	if err != nil {
		panic(fmt.Sprintf("Error: cannot read ca private key: %s\n", err.Error()))
	}

	block, _ := pem.Decode(CAPrivateKey)
	if block == nil {
		panic("failed to parse PEM block containing the key")
	}

	capriv, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		panic("aj znaj")
	}

	certBytes, err := x509.CreateCertificate(rand.Reader, cert, CACert, &certPrivKey.PublicKey, capriv)
	if err != nil {
		panic(fmt.Sprintf("Error: user cert: %s\n", err.Error()))
	}

	certPEM := new(bytes.Buffer)
	pem.Encode(certPEM, &pem.Block{
		Type:  "CERTIFICATE",
		Bytes: certBytes,
	})

	certPrivKeyPEM := new(bytes.Buffer)
	pem.Encode(certPrivKeyPEM, &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(certPrivKey),
	})

	err = ioutil.WriteFile(fmt.Sprintf("%s.crt", user.ID), certPEM.Bytes(), os.ModePerm)
	if err != nil {
		fmt.Println("Cannot write cert to file")
	}

	err = ioutil.WriteFile(fmt.Sprintf("%s.key", user.ID), certPrivKeyPEM.Bytes(), os.ModePerm)
	if err != nil {
		fmt.Println("Cannot write private key to file")
	}

	cb, err := x509.ParseCertificate(certBytes)
	pfxBytes, err := pkcs12.Encode(rand.Reader, certPrivKey, cb, []*x509.Certificate{CACert}, "secret")
	err = ioutil.WriteFile(fmt.Sprintf("%s.p12", user.ID), pfxBytes, os.ModePerm)
	if err != nil {
		fmt.Println("Cannot write private key to file")
	}
	return pfxBytes
}

func CreateUserCertificateEasy(user models.User, password string) string {
	fmt.Println("creating request")
	app := "./easyrsa"
	arg0 := "gen-req"
	arg1 := fmt.Sprintf("%s", user.Email)
	arg2 := "nopass"

	cmd := exec.Command(app, arg0, arg1, arg2)
	cmd.Dir = "easy-rsa"

	buffer := bytes.Buffer{}
	buffer.Write([]byte("\n")) //use default
	cmd.Stdin = &buffer

	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err := cmd.Run()
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
	}

	//./easyrsa sign-req client tmp
	fmt.Println("signing request")
	app = fmt.Sprintf("./easyrsa")
	arg0 = "sign-req"
	arg1 = "client"
	arg2 = fmt.Sprintf("%s", user.Email)

	cmd2 := exec.Command(app, arg0, arg1, arg2)
	cmd2.Dir = "easy-rsa"

	buffer2 := bytes.Buffer{}
	buffer2.Write([]byte("yes\n"))
	cmd2.Stdin = &buffer2

	cmd2.Stdout = os.Stdout
	cmd2.Stderr = os.Stderr

	err = cmd2.Run()
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
	}

	//openssl pkcs12 -export -out keyStore.p12 -inkey tmp.key -in tmp.crt

	fmt.Println("creating pkcs12")
	app = fmt.Sprintf("openssl")
	arg0 = "pkcs12"
	arg1 = "-export"
	arg2 = "-out"
	arg3 := fmt.Sprintf("pki/p12/%s.p12", user.Email)
	arg4 := "-inkey"
	arg5 := fmt.Sprintf("pki/private/%s.key", user.Email)
	arg6 := "-in"
	arg7 := fmt.Sprintf("pki/issued/%s.crt", user.Email)

	cmd3 := exec.Command(app, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7)
	cmd3.Dir = "easy-rsa"

	buffer3 := bytes.Buffer{}
	buffer3.Write([]byte(fmt.Sprintf("%s\n%s\n", password, password)))
	cmd3.Stdin = &buffer3

	cmd3.Stdout = os.Stdout
	cmd3.Stderr = os.Stderr

	err = cmd3.Run()
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
	}

	return fmt.Sprintf("easy-rsa/pki/p12/%s.p12", user.Email)
}
