#### Environment

```
cp chat-client/.env.example chat-client/.env
cp chat-login/.env.example chat-login/.env

cp chat-server/.env.example chat-server/.env
```
Set MAIL_USERNAME and MAIL_PASSWORD variables
```
sudo nano /etc/hosts
```
Add following line:
```
127.0.0.1 chat.example.com login.chat.example.com
```
#### Docker
```
docker-compose build
docker-compose up (-d)
```

#### Run
Go to https://chat.example.com

