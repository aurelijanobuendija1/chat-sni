import Vue from 'vue'
import axios from 'axios'

const axiosInstance = axios.create({
  baseURL: process.env.API_HOST,
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded'
  },
   crossdomain: true
})

Vue.prototype.$axios = axiosInstance

export { axiosInstance }
