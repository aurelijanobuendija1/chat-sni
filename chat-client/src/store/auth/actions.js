import {axiosInstance} from 'boot/axios'

const qs = require('querystring')
export default {
  AUTH_REQUEST({commit, dispatch}, user) {
    return new Promise((resolve, reject) => {
      commit('AUTH_REQUEST')
      axiosInstance.post(process.env.API_HOST + '/api/login', qs.encode(user))
        .then(resp => {
          commit('AUTH_SUCCESS', resp.data)
          resolve()
        })
        .catch(err => {
          commit('AUTH_ERROR', err)
          localStorage.removeItem('user-token')
          reject(err)
        })
    })
  },
  REGISTER_REQUEST({commit, dispatch}, user) {
    return new Promise((resolve, reject) => {
      axiosInstance.post(process.env.API_HOST + '/api/register', qs.encode(user), {responseType: 'arraybuffer'})
        .then(resp => {
          resolve(resp)
        })
        .catch(err => {
          if (err.response.status == 409) {
            reject("Email is already in use")
          }
          reject("Invalid data")
        })
    })
  },
  VERIFY_REQUEST({commit, dispatch}, payload) {
    return new Promise((resolve, reject) => {
      commit('VERIFY_REQUEST')
      axiosInstance.post(process.env.API_HOST + '/api/verify', qs.encode(payload))
        .then(resp => {
          commit('VERIFY_SUCCESS', resp.data)
          resolve()
        })
        .catch(err => {
          alert('verify error')
          commit('VERIFY_ERROR', err)
          localStorage.removeItem('user-token')
          reject(err)
        })
    })
  },
  PROFILE_REQUEST({commit, dispatch}, payload) {
    return new Promise((resolve, reject) => {
      commit('VERIFY_REQUEST')
      axiosInstance.get(process.env.API_HOST + '/api/profile')
        .then(resp => {
          commit('VERIFY_SUCCESS', resp.data)
          resolve()
        })
        .catch(err => {
          alert('verify error')
          commit('VERIFY_ERROR', err)
          localStorage.removeItem('user-token')
          reject(err)
        })
    })
  },
  AUTH_LOGOUT({commit, dispatch}) {
    return new Promise((resolve, reject) => {
      axiosInstance.get(process.env.API_HOST + '/api/logout')
        .then(resp => {
          commit('AUTH_LOGOUT')
          this.dispatch('socket/clearAllData')
          resolve()
        })
        .catch(err => {
          commit('AUTH_LOGOUT')
          this.dispatch('socket/clearAllData')
          resolve()
        })
      document.cookie = "session_id=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
      // location.reload()
    })
  }
}
