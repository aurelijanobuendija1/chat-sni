export default {
  isAuthenticated: state => !!state.token && state.hasLoadedOnce,
  isVerified: state => !!state.token && state.hasLoadedOnce,
  authStatus: state => state.status,
  getProfile: state => state.profile,
  token: state => state.token,
  temp_token: state => state.temp_token,
}
