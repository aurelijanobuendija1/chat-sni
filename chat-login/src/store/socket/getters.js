export default {
  getOnlineUsers: state => state.onlineUsers,
  getAllUsers: state => state.allUsers,
  isUserOnline: (state) => (user_id) => {
    return state.onlineUsers.includes(user_id)
  },
  getUserData: (state) => (user_id) => {
    if(state.allUsers.hasOwnProperty(user_id)) {
      return state.allUsers[user_id]
    }
    return {}
  },
  getActiveChats: (state) => { //TODO: needs refactor
    let messages = []
    for (let key in state.messages) {
      let last_message = state.messages[key].length > 0 ? state.messages[key][state.messages[key].length - 1].content : ""
      if(state.unreadMessages.hasOwnProperty(key) && state.unreadMessages[key].length > 0) {
        last_message = state.unreadMessages[key].length > 0 ? state.unreadMessages[key][state.unreadMessages[key].length - 1].content : ""
      }
      messages.push({
        key: key,
        user_full_name: state.allUsers[key].full_name,
        user_abbreviation: state.allUsers[key].abbreviation,
        number_of_unread_messages: state.unreadMessages[key] ? state.unreadMessages[key].length : 0,
        last_message: last_message
      })
    }
    for (let key in state.unreadMessages) {
      if(state.unreadMessages[key].length > 0 && !state.messages.hasOwnProperty(key)) {
        messages.push({
          key: key,
          user_full_name: state.allUsers[key].full_name,
          user_abbreviation: state.allUsers[key].abbreviation,
          number_of_unread_messages: state.unreadMessages[key] ? state.unreadMessages[key].length : 0,
          last_message: state.unreadMessages[key].length > 0 ? state.unreadMessages[key][state.unreadMessages[key].length - 1].content : ""
        })
      }
    }

    return messages
  },
  getMessagesFromChat: (state) => (user_id) => {
    return state.messages[user_id]
  },
  getUserFullName: (state) => (user_id) => {
    let user = state.allUsers[user_id]
    return user.full_name
  },
  getNumberOfNewChats: (state) => {
    return Object.keys(state.unreadMessages).filter(key => state.unreadMessages[key].length > 0).length
  }
}
