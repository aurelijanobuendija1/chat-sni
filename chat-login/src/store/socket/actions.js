export default {
  socket_onlineUsers ({ commit, dispatch }, users) {
    commit('SOCKET_ONLINE_USERS', JSON.parse(users))
  },
  socket_addOnlineUser ({ commit, dispatch }, user) {
    commit('SOCKET_ADD_ONLINE_USER', JSON.parse(user))
  },
  socket_removeOnlineUser ({ commit, dispatch }, user_id) {
    commit('SOCKET_REMOVE_ONLINE_USER', user_id)
  },
  socket_newMessage({commit, dispatch}, message) {
    commit('SOCKET_NEW_MESSAGE', JSON.parse(message))
  },
  sendNewMessage({commit, dispatch}, message) {
    commit('SEND_NEW_MESSAGE', JSON.parse(message))
  },
  setActiveChat({commit, dispatch}, user_id) {
    commit('SET_ACTIVE_CHAT', user_id)
  },
  clearAllData({commit,dispatch}) {
    commit('CLEAR_ALL_DATA')
  }
}
