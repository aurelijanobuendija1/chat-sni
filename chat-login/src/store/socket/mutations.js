export default {
  SOCKET_ONLINE_USERS: (state, users) => {
    Object.keys(users).map((key) => {
      let user = users[key]
      user.full_name = user.first_name + ' ' + user.last_name
      user.abbreviation = user.first_name.charAt(0) + user.last_name.charAt(0)

      state.allUsers[key] = user
      state.onlineUsers.push(user.id)
    })

  },
  SOCKET_ADD_ONLINE_USER: (state, user) => {
    if (!state.allUsers.hasOwnProperty(user.id)) {
      user.full_name = user.first_name + ' ' + user.last_name
      user.abbreviation = user.first_name.charAt(0) + user.last_name.charAt(0)

      let newList = {...state.allUsers}
      newList[user.id] = user
      state.allUsers = newList
    }
    if (!state.onlineUsers.includes(user.id)) {
      let newOnlineList = [...state.onlineUsers]
      newOnlineList.push(user.id)
      state.onlineUsers = newOnlineList
    }
  },
  SOCKET_REMOVE_ONLINE_USER: (state, user_id) => {
    let newList = [...state.onlineUsers]
    newList = newList.filter(item => item !== user_id)

    state.onlineUsers = newList
  },
  SET_ACTIVE_CHAT: (state, user_id) => {
    state.activeChat = user_id
    let read = state.messages.hasOwnProperty(user_id) ? state.messages[user_id] : []
    let unread = state.unreadMessages.hasOwnProperty(user_id) ? state.unreadMessages[user_id] : []

    if (read.length > 0 || unread.length > 0) {
      state.messages[user_id] = [...read, ...unread]

      let newUnread = state.unreadMessages
      newUnread[user_id] = []
      state.unreadMessages = {...newUnread}
    }
  },
  SOCKET_NEW_MESSAGE: (state, message) => {
    if (message.from) {
      if (state.activeChat === message.from) {
        let newList = {...state.messages}
        if (newList.hasOwnProperty(message.from)) {
          newList[message.from].push(message)
        } else {
          newList[message.from] = [message]
        }
        state.messages = newList
      } else {
        let newList = {...state.unreadMessages}
        if (newList.hasOwnProperty(message.from)) {
          newList[message.from].push(message)
        } else {
          newList[message.from] = [message]
        }
        state.unreadMessages = newList
      }
    }
  },
  SEND_NEW_MESSAGE: (state, message) => {
    let newList = {...state.messages}
    if (newList.hasOwnProperty(message.to)) {
      newList[message.to].push(message)
    } else {
      newList[message.to] = [message]
    }
    state.messages = newList
  },
  CLEAR_ALL_DATA: (state) => {
    state.isConnected = false
    state.allUsers = {}
    state.onlineUsers = []
    state.connectionId = {}
    state.messages = {}
    state.unreadMessages = {}
  }
}
