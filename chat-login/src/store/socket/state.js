export default function () {
  return {
    isConnected: false,
    allUsers: {},
    onlineUsers: [],
    connectionId: undefined,
    messages: {},
    unreadMessages: {},
    activeChat: undefined,
  }
}
