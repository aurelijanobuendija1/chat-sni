export default {
  AUTH_REQUEST: state => {
    state.status = 'loading'
  },
  AUTH_SUCCESS: (state, resp) => {
    state.auth_status = 'success'
    state.temp_token = resp.temp_token
    state.hasLoadedOnce = true
  },
  AUTH_ERROR: state => {
    state.auth_status = 'error'
    state.status = 'error'
    state.hasLoadedOnce = true
  },
  VERIFY_REQUEST: state => {
    state.status = 'loading'
  },
  VERIFY_SUCCESS: (state, resp) => {
    state.status = 'success'
    state.token = resp.session_id
    state.profile = resp.user
    state.profile.full_name = resp.user.first_name + ' ' + resp.user.last_name
    state.profile.abbreviation = resp.user.first_name.charAt(0) + resp.user.last_name.charAt(0)
    state.hasLoadedOnce = true
  },
  VERIFY_ERROR: state => {
    state.auth_status = 'error'
    state.status = 'error'
    state.hasLoadedOnce = true
  },
  AUTH_LOGOUT: state => {
    localStorage.removeItem('user-token')
    state.token = undefined
    state.temp_token = undefined
    state.hasLoadedOnce = false
    state.profile = {}
  }
}
