export default function () {
  return {
    temp_token: undefined,
    token: localStorage.getItem('user-token') || '',
    profile: {},
    status: '',
    auth_status: '',
    verify_status: '',
    hasLoadedOnce: false
  }
}
