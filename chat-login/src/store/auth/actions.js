import { axiosInstance } from 'boot/axios'

const qs = require('querystring')
export default {
  AUTH_REQUEST ({ commit, dispatch }, user) {
    return new Promise((resolve, reject) => {
      commit('AUTH_REQUEST')
      axiosInstance.post(process.env.API_HOST + '/api/login', qs.encode(user))
        .then(resp => {
          commit('AUTH_SUCCESS', resp.data)
          resolve()
        })
        .catch(err => {
          commit('AUTH_ERROR', err)
          localStorage.removeItem('user-token')
          reject(err)
        })
    })
  },
  REGISTER_REQUEST ({ commit, dispatch }, user) {
    return new Promise((resolve, reject) => {
      axiosInstance.post(process.env.API_HOST + '/api/register', qs.encode(user), {responseType: 'arraybuffer'})
        .then(resp => {
          resolve(resp)
        })
        .catch(err => {
          reject(err)
        })
    })
  },
  VERIFY_REQUEST ({ commit, dispatch }, payload) {
    return new Promise((resolve, reject) => {
      commit('VERIFY_REQUEST')
      axiosInstance.post(process.env.API_HOST + '/api/verify', qs.encode(payload))
        .then(resp => {
          window.location.href = process.env.REDIRECT
          // commit('VERIFY_SUCCESS', resp.data)
          // resolve()
        })
        .catch(err => {
          commit('VERIFY_ERROR', err)
          localStorage.removeItem('user-token')
          reject(err)
        })
    })
  },
  AUTH_LOGOUT ({ commit, dispatch }) {
    commit('AUTH_LOGOUT')
    this.dispatch('socket/clearAllData')
    // location.reload()
  }
}
