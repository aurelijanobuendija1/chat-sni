import Vue from 'vue'
import VueRouter from 'vue-router'

// import routes from './routes'
import { store } from '../store/index'

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters['auth/isAuthenticated']) {
    next()
    return
  }
  next('/')
}

const ifAuthenticatedAndVerified = (to, from, next) => {
  if (store.getters['auth/isAuthenticated'] && store.getters['auth/isVerified']) {
    next()
    return
  }
  next('/login')
}

const ifAuthenticatedAndNotVerified = (to, from, next) => {
  if (store.getters['auth/isAuthenticated'] && !store.getters['auth/isVerified']) {
    next()
    return
  }
  next('/login')
}


Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes: [
      {
        path: '/',
        component: () => import('layouts/MainLayout.vue'),
        children: [
          { path: '/', component: () => import('pages/PageBlank.vue'), beforeEnter: ifAuthenticatedAndVerified },
          { path: '/chat/:user_id', component: () => import('pages/PageChat.vue'), beforeEnter: ifAuthenticatedAndVerified },
        ]
      },
      {
        path: '/login',
        component: () => import('layouts/LoginLayout.vue'),
        children: [
          { path: '/', component: () => import('pages/PageLogin.vue'), beforeEnter: ifNotAuthenticated },
          { path: '/verify', component: () => import('pages/PageVerify.vue'), beforeEnter: ifAuthenticatedAndNotVerified },

        ],
        beforeEnter: ifNotAuthenticated,
      },
      {
        path: '*',
        component: () => import('pages/Error404.vue')
      }
    ],

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  return Router
}
