import io from 'socket.io-client'
import VueSocketIOExt from 'vue-socket.io-extended'

const socket = io(process.env.SOCKET_HOST, {
  autoConnect: false,
  // secure: false,
  transports: ['websocket'],
})


export default async ({store, Vue}) => {
  Vue.use(VueSocketIOExt, socket, {store})
}
